<?php

// Reads the variables sent via POST from our gateway
$sessionId = $_POST["sessionId"];
$serviceCode = $_POST["serviceCode"];
$phoneNumber = $_POST["phoneNumber"];
$text = $_POST["text"];


if ($text == "") {
    if ($phoneNumber != '+254726791019') {
        // This is the first request. Note how we start the response with CON
        $response = "END Hello, \nWelcome to KITI.\n";
        $response .= "It seems you are not a member yet. Kindly send an SMS with you details to 123454 to register \n";
        $response .= "FULL NAME#ID NO#COUNTY#";
    } else {
        $response = "CON Hello Mwalimu Hustler. \nWelcome to KITI.\n";
        $response .= "1. Enroll to a School \n";
        $response .= "2. My courses \n";
        $response .= "3. Fees & Loans \n";
        $response .= "4. Learning materials \n";
        $response .= "5. My Status \n";
        $response .= "00. Logout";
    }
} else if ($text == "1") {
    // Business logic for first level response
    $response = "CON Choose a school you want to enroll \n";
    $response .= "1. Kabach TTC \n";
    $response .= "2. Murang'a TTC \n";
    $response .= "3. Elimu Bora TTC";
} else if ($text == "2") {
    $response = "END Enrolled courses \n";
    $response = "1. Calculus I \n";
    $response = "2. Basic Math I \n";
    $response = "3. English Module I";

} else if ($text == "3") {
    $response = "CON";
    $response = "1. School balance \n";
    $response = "2. Loans \n";
    $response = "0. Back";

} else if ($text == "4") {
    $response = "CON";
    $response = "1. By email \n";
    $response = "2. By ext \n";
    $response = "0. Back";

} else if ($text == "5") {
    $response = "CON";
    $response = "1. By email \n";
    $response = "2. By text \n";
    $response = "0. Back";

} else if ($text == "00") {
    $response = "END Thank you for testing KITI. Have a good one";

} else if ($text == "1*1") {
    $accountNumber = "ACC1001";
    $response = "END Your enrollment to  Kabach TTC is being processed. You will get a text notification once done. Your KITI enrollement ID id $accountNumber Thank you.";
} else if ($text == "1*2") {
    $accountNumber = "ACC1002";
    $response = "END Your enrollment to  Murang'a TTC is being processed. You will get a text notification once done. Your KITI enrollement ID id $accountNumber Thank you.";
} else if ($text == "1*3") {
    $accountNumber = "ACC1003";
    $response = "END Your enrollment to Elimu Bora TTC is being processed. You will get a text notification once done. Your KITI enrollement ID id $accountNumber Thank you.";
} else if ($text == "3*1") {
    $bal = "Ksh 9850.00";
    $response = "CON Your withstanding school balance is $bal. You can apply for our loan to clear the balance. Thank you.\n 0. Back \n 00. Logout";
} else if ($text == "3*2") {
    $response = "CON You have not yet enrolled to our loans.\n 1. Enroll \n0. Back \n 00. Logout";
}else if ($text == "4*1") {
    $response = "CON Select unit below.\n";
    $response = "1. Calculus I \n";
    $response = "2. Basic Math I \n";
    $response = "3. English Module I";
}
// Print the response onto the page so that our gateway can read it
header('Content-type: text/plain');
echo $response;
// DONE!!!